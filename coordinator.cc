
////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////

#include <cstdlib>
#include <czmq.h>
#include <string>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <utility>
#include "format.h"

using namespace std;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::string;

using std::set;
using std::pair;
using fmt::print;

using Coordinate = pair<size_t, size_t>;

class Sudoku {
private:
  using Cell = set<int>;
  using Board = vector<vector<Cell>>;


public:

  Board board;

  Sudoku() {
    Cell all{1, 2, 3, 4, 5, 6, 7, 8, 9};
    board = {
        {all, all, all, all, all, all, all, all, all},
        {all, all, all, all, all, all, all, all, all},
        {all, all, all, all, all, all, all, all, all},
        {all, all, all, all, all, all, all, all, all},
        {all, all, all, all, all, all, all, all, all},
        {all, all, all, all, all, all, all, all, all},
        {all, all, all, all, all, all, all, all, all},
        {all, all, all, all, all, all, all, all, all},
        {all, all, all, all, all, all, all, all, all},
    };
  }

  Sudoku(const vector<vector<int>>& s) {
    Cell all{1, 2, 3, 4, 5, 6, 7, 8, 9};
    for (const auto& row : s) {
      board.push_back(vector<Cell>());
      for (const auto& v : row) {
        if (v == 0)
          board.back().push_back(all);
        else
          board.back().push_back({v});
      }
    }
  }


    Sudoku(const vector<vector<int>>& s,int i) {
    Cell all{};
    for (const auto& row : s) {
      board.push_back(vector<Cell>());
      for (const auto& v : row) {
        if (v == 0)
          board.back().push_back(all);
        else
          board.back().push_back({v});
      }
    }
  }


  Sudoku(const Sudoku& other)
      : board(other.board) {}

  bool isSolved() const {
    for (const auto& row : board)
      for (const auto& cell : row)
        if (cell.size() > 1) return false;
    return true;
  }

  bool isFailed() const {
    for (const auto& row : board)
      for (const auto& cell : row)
        if (cell.size() == 0) return true;
    return false;
  }

  bool solvedCell(size_t i, size_t j) const { return board[i][j].size() == 1; }

  int valueCell(size_t i, size_t j) const {
    assert(solvedCell(i, j));
    return *(board[i][j].begin());
  }

  void reduce() {
    bool r = true;
    while (r) {
      r = false;
      for (size_t x = 0; x < 9; x++)
        for (size_t y = 0; y < 9; y++) {
          r = r || reduceRow(x, y);
          r = r || reduceCol(x, y);
          r = r || reduceBox(x, y);
        }
      // fmt::print_colored(fmt::YELLOW, "Finished pass\n");
    }
  }

  Coordinate nextCellTosolve() const {
    for (size_t x = 0; x < 9; x++)
      for (size_t y = 0; y < 9; y++)
        if (!solvedCell(x, y)) return {x, y};
    assert(false);
    return {0, 0};
  }

  Coordinate smarterNextCellTosolve() const {
    size_t minCard = 9;
    Coordinate result{0, 0};
    for (size_t x = 0; x < 9; x++)
      for (size_t y = 0; y < 9; y++) {
        if (!solvedCell(x, y) && board[x][y].size() < minCard) {
          minCard = board[x][y].size();
          result = {x, y};
        }
      }
    return result;
  }

  int possibleValueForCell(const Coordinate& c) const {
    return *(board[c.first][c.second].begin());
  }

  void removeValueForCell(const Coordinate& c, int v) {
    board[c.first][c.second].erase(v);
  }

  void assignValueForCell(const Coordinate& c, int v) {
    board[c.first][c.second] = {v};
  }

private:
  /**
   * Reduces the cell (i,j) using the row property.
   */
  bool reduceRow(size_t i, size_t j) {
    auto oldSize = board[i][j].size();
    for (size_t x = 0; x < 9; x++) {
      if (x != j && solvedCell(i, x)) {
        // fmt::print_colored(fmt::BLUE, "using {} {} to reduce {} {}\n", i, x,
        // i,j);
        board[i][j].erase(valueCell(i, x));
      }
    }
    auto newSize = board[i][j].size();
    if (newSize < oldSize) {
      // fmt::print_colored(fmt::RED, "Effective row reduction of ({} {})\n", i,
      //                    j);
      return true;
    }
    return false;
  }

  /**
   * Reduces the cell (i,j) using the column property.
   */
  bool reduceCol(size_t i, size_t j) {
    // fmt::print_colored(fmt::RED, "Reducing cell ({},{})\n", i, j);
    auto oldSize = board[i][j].size();
    for (size_t row = 0; row < 9; row++) {
      if (row != i && solvedCell(row, j)) {
        board[i][j].erase(valueCell(row, j));
        // fmt::print("I will use {} {}\n", row, j);
      }
    }
    auto newSize = board[i][j].size();
    if (newSize < oldSize) {
      // fmt::print_colored(fmt::RED, "Effective col reduction of ({} {})\n", i,
      //                    j);
      return true;
    }
    return false;
  }

  bool reduceBox(size_t i, size_t j) {
    auto oldSize = board[i][j].size();

    size_t boxStartRow = i - (i % 3);
    size_t boxStartCol = j - (j % 3);

    for (size_t row = 0; row < 3; row++)
      for (size_t col = 0; col < 3; col++) {
        size_t x = row + boxStartRow;
        size_t y = col + boxStartCol;
        if (!(x == i && y == j) && solvedCell(x, y)) {
          board[i][j].erase(valueCell(x, y));
        }
      }
    auto newSize = board[i][j].size();
    if (newSize < oldSize) {
      // fmt::print_colored(fmt::RED, "Effective box reduction of ({} {})\n", i,
      //                    j);
      return true;
    }
    return false;
  }

public:
  void print() const {
    fmt::print("Sudoku\n");
    size_t i = 0;
    for (const auto& row : board) {
      size_t j = 0;
      if (i % 3 == 0) fmt::print("{:-^13}\n", "");
      for (const auto& cell : row) {
        if (j % 3 == 0) fmt::print("|");

        if (cell.size() == 0) {
          fmt::print_colored(fmt::RED, "{{}}");
        } else if (cell.size() == 1) {
          auto elem = *(cell.cbegin());
          fmt::print_colored(fmt::GREEN, "{}", elem);
        } else {
          fmt::print("{{");
          for (auto i : cell) fmt::print("{},", i);
          fmt::print("}}");
        }
        j++;
      }
      fmt::print("|\n");
      i++;
    }
    fmt::print("{:-^13}\n", "");
  }
};

struct Statistics {
  size_t solutions;
  size_t failures;
  size_t decisions;
  size_t reductions;

  Statistics()
      : solutions(0)
      , failures(0)
      , decisions(0)
      , reductions(0) {}
  void print() const {
    fmt::print_colored(
        fmt::GREEN,
        "Solutions: {}\t Failures: {}\t Decisions: {}\t Reductions: {}\n",
        solutions, failures, decisions, reductions);
  }
};



void sendMsg(void* channel, zframe_t* to, vector<string> parts) {
  zmsg_t* msg = zmsg_new();
  zframe_t* dto = zframe_dup(to);
  zmsg_append(msg, &dto);
  for (const string& s : parts) {
    zmsg_addstr(msg, s.c_str());
  }
  zmsg_send(&msg, channel);
}

/*
void sendMsg(void* channel, vector<string> parts) {
  zmsg_t* msg = zmsg_new();
  for (const string& s : parts) {
    zmsg_addstr(msg, s.c_str());
  }
  zmsg_send(&msg, channel);
}
*/

string intToS(int n)
{
  std::ostringstream sin;
  sin << n;
  std::string val = sin.str();
  return val;
}

vector<string> serialize(Sudoku& copy,int priority)
  {
    vector<string> message;
    message.push_back(to_string(priority));
    int formater = 0; 

    for(auto i: copy.board)
      {
        for(auto j: i){
          if(formater%9==0)
            {
              cout<<endl;
            }
        
          for(auto o : j)
            {
              cout << o ;
              message.push_back(to_string(o));    
            }
          cout<<"|";
          message.push_back("|");
          formater++;
        }
      }

    return  message;
  }




pair<Sudoku, bool> solveOne(Sudoku& s, Statistics& st,void* channel, zframe_t* to,int priority) {

  vector<string> message1;
  vector<string> message2;

  s.reduce();
  st.reductions++;

  if (s.isFailed()) {
    st.failures++;
    return {s, false};
  } else if (s.isSolved()) {
    st.solutions++;
    return {s, true};
  } else {

    st.decisions++;
    s.print();
    
    message2 = serialize(s,priority);

    
    cout<<"TO SEND"<<endl;

    for(auto ite: message2){
        cout<<ite;
      }


    s.print();



    Sudoku copy(s);
    // Make a decision: find the next cell to be solved and assign one of its
    // possible values to it.
    // Coordinate next = copy.nextCellTosolve();
    Coordinate next = copy.smarterNextCellTosolve();
    int val = copy.possibleValueForCell(next);
    copy.assignValueForCell(next, val);
    cout << "asignado"<<endl;
    copy.print();


    copy.reduce();

    //PRIMER MSG
    cout << "primer"<<endl;
    copy.print();

    cout << "messages"<<endl;


    message1 = serialize(copy,priority+1);

    sendMsg(channel, to, message1);
    /*cout<<"TO SEND"<<endl;

    for(auto ite: message1)
      {
        cout<<ite;
      }*/

     //primero.removeValueForCell(next, val);

    
      return {copy,false};
    //}
  }
}

vector<Sudoku> solveAll(Sudoku& s, Statistics& st) {
  s.reduce();
  st.reductions++;

  if (s.isFailed()) {
    st.failures++;
    return {};
  }
  if (s.isSolved()) {
    st.solutions++;
    return {s};
  }

  st.decisions++;
  Sudoku copy(s);
  // Make a decision: find the next cell to be solved and assign one of its
  // possible values to it.
  // Coordinate next = copy.nextCellTosolve();
  Coordinate next = copy.smarterNextCellTosolve();
  int val = copy.possibleValueForCell(next);
  copy.assignValueForCell(next, val);
  vector<Sudoku> result = solveAll(copy, st);

  s.removeValueForCell(next, val);
  vector<Sudoku> result2 = solveAll(s, st);

  // Aggregate all the solutions
  result.insert(result.end(), result2.begin(), result2.end());
  return result;
}


Sudoku deserialize_sudoku(zmsg_t* sudoku)
  {
                            /////SUDOKU DESERIALIZE.
                      //char* prioridad = zmsg_popstr(sudoku);
                      //char* prioridad = zmsg_popstr(sudoku);


                      int formater = 0;

                      Sudoku deserilized_sudoku({{{}, {}, {}, {}, {}, {}, {}, {}, {}},
                                                  {{}, {}, {}, {}, {}, {}, {}, {}, {}},
                                                  {{}, {}, {}, {}, {}, {}, {}, {}, {}},
                                                  {{}, {}, {}, {}, {}, {}, {}, {}, {}},
                                                  {{}, {}, {}, {}, {}, {}, {}, {}, {}},
                                                  {{}, {}, {}, {}, {}, {}, {}, {}, {}},
                                                  {{}, {}, {}, {}, {}, {}, {}, {}, {}},
                                                  {{}, {}, {}, {}, {}, {}, {}, {}, {}},
                                                  {{}, {}, {}, {}, {}, {}, {}, {}, {}}},0);


                      size_t tam = zmsg_size(sudoku);

                      //cout<<"CONVERTIR ESTA cosa"<<endl;

                      int counter = 0;

                      int k = 0;
                      int j = 0;

                      set<int> new_cell;
                      deserilized_sudoku.board[counter%9][counter/9]=new_cell;

                      for (int i = 0; i < tam; ++i)
                       {

                        char* current_part= zmsg_popstr(sudoku);
                         

                              if(!strcmp(current_part,"|"))
                                {
                                  set<int> new_cell;
                                  counter++;
                                  if (counter<81)
                                {
                                  
                                  deserilized_sudoku.board[counter/9][counter%9]=new_cell;
                                }
                                }

                                //counter++;
                               cout<<"parte:  "<<current_part<<endl;
                               //cout << "msg tam"<<tam<<endl;
                               cout << "contador"<<counter<<endl;
                               cout << "indices" <<"i:  "<< counter%9 <<"j: "<< counter/9 <<endl;
                                if (counter<81)
                                {

                                    cout<<"PIECE OF SHIT."<<atoi(current_part)<<endl;
                                    
                                    
                                    if(strcmp(current_part,"|"))
                                  {
                                    deserilized_sudoku.board[counter/9][counter%9].insert(atoi(current_part));
                                    
                                  }
                                }                            
                                  
                          
                       }

        return deserilized_sudoku;
  }




int main ()
{
    fmt::print("Sudoku solver\n");

  zctx_t* context = zctx_new();
  void* client = zsocket_new(context, ZMQ_DEALER);

  void* listener = zsocket_new(context,ZMQ_ROUTER);
  
  //zsocket_connect(client, "tcp://192.168.8.214:5555");
  zsocket_connect(client, "tcp://localhost:5555");
  zmq_bind(listener, "tcp://*:5555");

  //ASYNC EVENT POOL

    //zmq_pollitem_t items[] = {{listener, 0, ZMQ_POLLIN, 0}};  
  
    map<zframe_t*, bool> workers;

    bool flag = false;

    /*sendMsg(client, {"join"});
    zmsg_t* msg = zmsg_recv(listener);
    zframe_t* Main_id = zmsg_pop(msg);
	*/


  Sudoku b({{2, 0, 0, 0, 8, 0, 3, 0, 0},
            {0, 6, 0, 0, 7, 0, 0, 8, 4},
            {0, 3, 0, 5, 0, 0, 2, 0, 9},
            {0, 0, 0, 1, 0, 5, 4, 0, 8},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {4, 0, 2, 7, 0, 6, 0, 0, 0},
            {3, 0, 1, 0, 0, 7, 0, 4, 0},
            {7, 2, 0, 0, 4, 0, 0, 6, 0},
            {0, 0, 4, 0, 1, 0, 0, 0, 3}});

  int prioridad = 0 ;


//MESSAGE STUFF/
    /*auto result = solveOne(b,st);

    result.first;
    result.second;

    cout <<"MAIN MAIN MAIN"<<endl;
    for(auto it:result.first)
      {
        cout<<it;
      }
    cout <<"-----------------------"<<endl;
    for(auto it:result.se)
      {
        cout<<it;
      }
*/

    while(1)
      {
/*      		if(flag == true)     			{
      				 sendMsg(client, {"WORK MUTHERFUCKER."});
      			}*/
        if(workers.size() == 0)
        {
		            	cout << "waiting for workers to join."<< endl;
		            
			            zmsg_t* msg = zmsg_recv(listener);

			            zframe_t* id = zmsg_pop(msg);
			            zframe_t* action = zmsg_pop(msg);

			            if(zframe_streq(action,"join"))
			            	{
			            		cout << "sender "<< id << endl;
			            		workers[id]=false;
			            		sendMsg(listener, id, {"ok"});
			            		//flag = true;
			            	}
			            

              ///INITIALIZE WORK TO SEND.

			            zmsg_destroy(&msg);
            }
        else
        	{
                //VALIDATE IF THE IS MORE WORKERS TO ADD.

        			  cout << "waiting for workers to ask for work."<< endl;

        			  zmsg_t* msg = zmsg_recv(listener);

			          zframe_t* id = zmsg_pop(msg);
                zframe_t* action = zmsg_pop(msg);
                
			          if(zframe_streq(action,"iddle"))
                    {
       						
                    //////RECIBIR CAMELLO Y HACER LO Q HAY Q HACER. JAJAJ                  	
						/*zframe_t* typeT =zmsg_pop(msg);
                    	cout<<"GONORREA"<<type<<endl;*/

                    if(workers[id] == false)
                      {

                        /////SEND WORK
                        cout<< " SENDING WORK"<<endl;
  
                        sendMsg(listener, id, {"work"});
                        Statistics st;
                        solveOne(b,st,listener,id,prioridad);
                        workers[id] = true;
                      }

                    }

                if(zframe_streq(action,"sudoku"))
							{
								zmsg_t* sudoku = zmsg_recv(listener);
								zframe_t* id =  zmsg_pop(sudoku);
								

								int prioridad = (int)zmsg_popstr(sudoku);
								Sudoku deserialized_sudoku = deserialize_sudoku(sudoku);
								cout<< "DAMM DAMM DAMM DAMM"<<endl;
								zmsg_print(sudoku);
								deserialized_sudoku.print();
							}
            

                    //WAIT FOR MORE WORKERS.
                if(zframe_streq(action,"join"))
                    {
                        cout << "sender "<< id << endl;
                        workers[id] = false;
                        sendMsg(listener, id, {"ok"});
                    }

        	}
    	
}      

    zmq_close(&listener);
     zctx_destroy(&context);
  return 0;
}
