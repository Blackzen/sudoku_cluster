CC=g++ -std=c++11

ZMQ=/home/utp/zmq
SFML=/usr/local

ZMQ_INCS=$(ZMQ)/include
ZMQ_LIBS=$(ZMQ)/lib
SFML_INCS=$(SFML)/include
SFML_LIBS=$(SFML)/lib

all:sudoku sudoku-test coordinator worker 

sudoku: sudoku.cc
	$(CC) -o sudoku sudoku.cc format.cc

sudoku-test: sudoku-test.cc
	$(CC) -o sudoku-test sudoku-test.cc format.cc 


coordinator: coordinator.cc
	$(CC) -o coordinator coordinator.cc -lczmq -lzmq format.cc 

worker: worker.cc
	$(CC) -o worker worker.cc -lczmq -lzmq format.cc

clean:
	rm -f coordinator 
	rm -f worker
	rm -f sudoku
	rm -f sudoku-test  